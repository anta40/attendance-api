package models

type CheckinParam struct {
	UserId       string `form:"id" json:"id"`
	UserLocation string `form:"location" json:"location"`
	UserDistance int    `form:"distance" json:"distance"`
}

type CheckinResponse struct {
	Code    int    `json:"status"`
	Message string `json:"message"`
}
