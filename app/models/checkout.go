package models

type CheckoutParam struct {
	UserId       string `form:"id" json:"id"`
	UserLocation string `form:"location" json:"location"`
	UserDistance int    `form:"distance" json:"distance"`
}

type CheckoutResponse struct {
	Code    int    `json:"status"`
	Message string `json:"message"`
}
