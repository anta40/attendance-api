package models

type UserParam struct {
	Id   string `form:"id" json:"id"`
	Date string `form:"id" json:"date"`
}

type UserStatus struct {
	LoggedIn   bool `form:"loggedin" json:"loggedin"`
	CheckedIn  bool `form:"checkedin" json:"checkedin"`
	CheckedOut bool `form:"checkedout" json:"checkout"`
}
