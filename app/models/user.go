package models

type User struct {
	UserId    string `form:"userid" json:"userid"`
	UserName  string `form:"username" json:"username"`
	UserEmail string `form:"email" json:"email"`
}
