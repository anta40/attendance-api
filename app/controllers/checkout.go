package controllers

import (
	"attendance-api/app/models"
	"encoding/json"
	"time"

	"github.com/revel/revel"
)

type Checkout struct {
	*revel.Controller
}

func (c Checkout) DoCheckout() revel.Result {

	var tmpcheckintime string
	var tmpcheckouttime string
	var param models.CheckoutParam
	var resp models.CheckoutResponse

	json.NewDecoder(c.Request.GetBody()).Decode(&param)

	db := DB_Connect()
	defer db.Close()

	query1 := `SELECT checkin_time FROM oc_log where userid=$1 and userdate=$2`
	db.QueryRow(query1, param.UserId, time.Now().Format("01-02-2006")).Scan(&tmpcheckintime)

	if len(tmpcheckintime) == 0 {
		query2 := `SELECT checkout_time FROM oc_log where userid=$1 and userdate=$2`
		db.QueryRow(query2, param.UserId, time.Now().Format("01-02-2006")).Scan(&tmpcheckouttime)

		if len(tmpcheckouttime) == 0 {
			checkoutQuery := `INSERT INTO oc_log (userid, userdate, checkout_time, checkout_location, checkout_distance) VALUES ($1, $2, $3, $4, $5) returning userid`
			_, err2 := db.Exec(checkoutQuery, param.UserId, time.Now().Format("01-02-2006"), time.Now().Format("15:04:05"),
				param.UserLocation, param.UserDistance)

			if err2 != nil {
				resp.Code = 500
				resp.Message = "Invalid checkout"
			} else {
				resp.Code = 200
				resp.Message = "Successfully checked out"
			}
		} else {
			resp.Code = 500
			resp.Message = "Already checked out"
		}

	} else {
		checkoutQuery := `UPDATE oc_log SET checkout_time = $1, checkout_location = $2, checkout_distance = $3 WHERE userid = $4 AND userdate = $5;`

		_, err2 := db.Exec(checkoutQuery, time.Now().Format("15:04:05"),
			param.UserLocation, param.UserDistance, param.UserId, time.Now().Format("01-02-2006"))

		if err2 != nil {
			resp.Code = 500
			resp.Message = "Invalid checkout"
		} else {
			resp.Code = 200
			resp.Message = "Successfully checked out"
		}

	}

	defer c.Request.Destroy()
	return c.RenderJSON(resp)

}
