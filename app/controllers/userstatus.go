package controllers

import (
	"attendance-api/app/models"
	"encoding/json"

	"github.com/revel/revel"
)

type UserStatus struct {
	*revel.Controller
}

func (c UserStatus) DoGetStatus() revel.Result {
	var param models.UserParam
	var resp models.UserStatus

	var b_isLoggedIn bool
	var b_hasCheckedIn bool
	var b_hasCheckOut bool

	var tmpDate string
	var tmpCheckedIn string
	var tmpCheckedOut string

	json.NewDecoder(c.Request.GetBody()).Decode(&param)

	db := DB_Connect()
	defer db.Close()

	query := `SELECT a.userdate, b.checkin_time, b.checkout_time FROM oc_token a JOIN oc_log b ON a.userid = b.userid AND a.userdate = b.userdate 
	WHERE a.userdate = $1 AND a.userid = $2`
	row := db.QueryRow(query, param.Date, param.Id)
	row.Scan(&tmpDate, &tmpCheckedIn, &tmpCheckedOut)

	if tmpDate == "" {
		b_isLoggedIn = false
	} else {
		b_isLoggedIn = true
	}

	if tmpCheckedIn == "" {
		b_hasCheckedIn = false
	} else {
		b_hasCheckedIn = true
	}

	if tmpCheckedOut == "" {
		b_hasCheckOut = false
	} else {
		b_hasCheckOut = true
	}

	resp.LoggedIn = b_isLoggedIn
	resp.CheckedIn = b_hasCheckedIn
	resp.CheckedOut = b_hasCheckOut

	defer c.Request.Destroy()
	return c.RenderJSON(resp)
}
