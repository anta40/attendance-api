package controllers

import (
	"attendance-api/app/models"
	"crypto/md5"
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"log"
	"time"

	_ "github.com/lib/pq"
	"github.com/revel/revel"
	"golang.org/x/crypto/bcrypt"
)

const (
	db_host     = "localhost"
	db_port     = 2019
	db_user     = "postgres"
	db_password = "dbadmin"
	db_name     = "oc-indo-dev"
)

type Login struct {
	*revel.Controller
}

func (c Login) DoLogin() revel.Result {
	var param models.LoginParam
	var resp models.LoginResponse
	var username string
	var userid string
	var user models.User
	var tmpId string
	var userToken string

	err := json.NewDecoder(c.Request.GetBody()).Decode(&param)

	if err != nil {
		log.Fatal("JSON decode error: ", err)
	}

	query := `SELECT userid, username FROM oc_user WHERE email=$1 AND password=$2`
	db := DB_Connect()
	row := db.QueryRow(query, param.Email, param.Password)

	switch err := row.Scan(&userid, &username); err {
	case sql.ErrNoRows:
		resp.Code = 500
		resp.Message = "Invalid login"
		user = models.User{UserId: "", UserName: "", UserEmail: ""}
		resp.Data = user
	case nil:
		resp.Code = 200
		resp.Message = "Succesfully logged in"
		userToken = GenerateToken(param.Email, param.Password)
		user = models.User{UserId: userid, UserName: username, UserEmail: param.Email}
		resp.Data = user

		query2 := `INSERT INTO oc_token (userid, usertoken, userdate) VALUES ($1, $2, $3) returning userid`
		db.QueryRow(query2, userid, userToken, time.Now().Format(time.RFC3339)).Scan(&tmpId)
	default:
		panic(err)
	}

	defer c.Request.Destroy()
	return c.RenderJSON(resp)
}

func GenerateToken(email string, password string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(email), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
	}

	hash2, err2 := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err2 != nil {
		log.Fatal(err2)
	}

	hasher := md5.New()
	hash = append(hash, hash2...)
	hasher.Write(hash)
	return hex.EncodeToString(hasher.Sum(nil))
}
