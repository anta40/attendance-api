package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"attendance-api/app/models"

	_ "github.com/lib/pq"
	"github.com/revel/revel"
)

type Checkin struct {
	*revel.Controller
}

func DB_Connect() *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		db_host, db_port, db_user, db_password, db_name)
	db, err := sql.Open("postgres", psqlInfo)

	if err != nil {
		log.Fatal(err)
	}

	return db
}

func (c Checkin) DoCheckin() revel.Result {
	var tmpcheckintime string
	var resp models.CheckinResponse
	var param models.CheckinParam
	var tmpid string

	json.NewDecoder(c.Request.GetBody()).Decode(&param)

	db := DB_Connect()

	query1 := `SELECT checkin_time FROM oc_log WHERE userid=$1 AND userdate=$2`
	db.QueryRow(query1, param.UserId, time.Now().Format("01-02-2006")).Scan(&tmpcheckintime)

	if len(tmpcheckintime) > 0 {
		resp.Code = 500
		resp.Message = "Already checked in"

	} else {
		query2 := `INSERT INTO oc_log (userid, userdate, checkin_time, checkin_location, checkin_distance) VALUES ($1, $2, $3, $4, $5) returning userid`
		err2 := db.QueryRow(query2, param.UserId, time.Now().Format(time.RFC3339), time.Now().Format("15:04:05"),
			param.UserLocation, param.UserDistance).Scan(&tmpid)

		if err2 != nil {
			resp.Code = 500
			resp.Message = "Invalid checkin"
		} else {
			resp.Code = 200
			resp.Message = "Successfully checked in"
		}
	}

	defer c.Request.Destroy()
	return c.RenderJSON(resp)
}
